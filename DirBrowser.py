#!/usr/bin/env python3

""" make my own Folder and/or File select popup window
thats because the original popup gives me
- hidden folder and files in the list
- raises permission errors
"""

import argparse
import fnmatch
import os
import sys
from pathlib import Path

import PySimpleGUI as sg


# noinspection PyTypeChecker
def PopupGetFileOrFolder(message='', title='', initial_folder='', not_higher_than='',
                         filepattern='',
                         theme='Default',
                         directory_only=True,
                         selection_mode=True,
                         show_hidden=False,
                         with_all_subs=False):
    """ Displays a PySimpleGUI Popup Dialog which is multi selecable
    :param selection_mode: determinates if the selection checkbox is visible, in this case files AND folders are
    selectable.
    :param filepattern: a list of given filenamepattern like *.txt, *, .*
    :param message: displays a message inside the dialog default is: not set
    and in this case there will be the given folder path displayed
    :param title: to be displayed
    :param initial_folder: as it say is the folder form which all files and subfolders are displayed default is: empty
    :param not_higher_than: determinates the highest path to go up to with the UP button default is: empty
    :param theme: is the color theme string which could be filled with PySimpleGUI theme_list() strings
    default is: Default
    :param directory_only: if set only directorys are shown. You got a directory chooser otherwise it's a filechooser
    :param show_hidden: if set hidden files and directorys are shown. Default is: False
    :param with_all_subs: if set all subdirectorys are read and insert as objects - that causes a big carbage collection
    of tkinter objects when close the dialog
    :return: you get back a list of selected directory/filenames.
    """

    # this is from the TreeElement Example on PySimpeGUI - Github
    # Base64 versions of images of a folder and a file. PNG files (may not work with PySimpleGUI27, swap with GIFs)
    # popup_icon is free licensed under : https://www.iconfinder.com/search/?price=free&style=flat&q=files&size=128
    popup_icon = b'iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAABeElEQVR4nO3asUrEQBQF0NGXXU1CwK/wE/wN01r5VeLMCo' \
                 b'qF2loJgromYOt3mM7Wwrk2BoVdd8Nuxkd27oXXv3vINJMxhmGMMcbASQkrjbeCPgZO3uFGx9q9OqfP8r8QPjAx+9rdOqXv8' \
                 b'j8IyQNgtrT7LU0oAG8FOBsfafdbmqAATt5wYfa0Oy5MSABvBZ9WTrQ7LkxoADjxOE0PtHv+mdAA3gpgt18xNYl217n5D4Cwu' \
                 b'NLASRktQIsQNYC3AgIQgAAEIAABCBAQ4GoEPKZAlfUz0xT+ZjwcgF7Lt/OcwZ8nAwHou3w7d7uRA1QZ/PUobgA8pfCT9Y7Cs' \
                 b'AGqDLjdiRygyuAvV/8KNgIA9ym8ixkg5NR5g7qYvTmKBuAbIW6AKps9KgQgAAEIQAACEIAABCBApABdn8lpLx4OoONDSe3Fg' \
                 b'wF0jfbiBCAAAQhAAAIQgAAEIAABCEAAAhBAAaDOG+3l1555v8e7AxTloBHqvMFLcbgyALNh+QItGlMv/kddWAAAAABJRU5E' \
                 b'rkJggg=='
    folder_icon = b'iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAACXBIWXMAAAsSAAALEgHS3X78AAABnUlEQVQ4y8WSv2rUQRSFv' \
                  b'7vZgJFFsQg2EkWb4AvEJ8hqKVilSmFn3iNvIAp21oIW9haihBRKiqwElMVsIJjNrprsOr/5dyzml3UhEQIWHhjmcpn7zblw' \
                  b'4B9lJ8Xag9mlmQb3AJzX3tOX8Tngzg349q7t5xcfzpKGhOFHnjx+9qLTzW8wsmFTL2Gzk7Y2O/k9kCbtwUZbV+Zvo8Md3PALr' \
                  b'joiqsKSR9ljpAJpwOsNtlfXfRvoNU8Arr/NsVo0ry5z4dZN5hoGqEzYDChBOoKwS/vSq0XW3y5NAI/uN1cvLqzQur4MCpBGEE' \
                  b'd1PQDfQ74HYR+LfeQOAOYAmgAmbly+dgfid5CHPIKqC74L8RDyGPIYy7+QQjFWa7ICsQ8SpB/IfcJSDVMAJUwJkYDMNOEPI' \
                  b'BxA/gnuMyYPijXAI3lMse7FGnIKsIuqrxgRSeXOoYZUCI8pIKW/OHA7kD2YYcpAKgM5ABXk4qSsdJaDOMCsgTIYAlL5TQF' \
                  b'TyUIZDmev0N/bnwqnylEBQS45UKnHx/lUlFvA3fo+jwR8ALb47/oNma38cuqiJ9AAAAAASUVORK5CYII= '
    file_icon = b'iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAACXBIWXMAAAsSAAALEgHS3X78AAABU0lEQVQ4y52TzStEURiHn' \
                b'/ecc6XG54JSdlMkNhYWsiILS0lsJaUsLW2Mv8CfIDtr2VtbY4GUEvmIZnKbZsY977Uwt2HcyW1+dTZvt6fn9557BGB' \
                b'+aaNQKBR2ifkbgWR+cX13ubO1svz++niVTA1ArDHDg91UahHFsMxbKWycYsjze4muTsP64vT43v7hSf/A0FgdjQPQWAmco68nB' \
                b'+T+SFSqNUQgcIbN1bn8Z3RwvL22MAvcu8TACFgrpMVZ4aUYcn77BMDkxGgemAGOHIBXxRjBWZMKoCPA2h6qEUSRR2MF6GxUUM' \
                b'UaIUgBCNTnAcm3H2G5YQfgvccYIXAtDH7FoKq/AaqKlbrBj2trFVXfBPAea4SOIIsBeN9kkCwxsNkAqRWy7+B7Z00G3xVc2wZ' \
                b'eMSI4S7sVYkSk5Z/4PyBWROqvox3A28PN2cjUwinQC9QyckKALxj4kv2auK0xAAAAAElFTkSuQmCC '

    # this is the placeholder for the information returned by the function
    back = None

    # magic happens here
    def make_treedata_from_path(path, min_root='/', findpattern='', only_dirs=False, hidden_path=False,
                                include_all_sub=False):
        """
        :param findpattern: like *.txt
        :param path: a given start point inside the os directory structure
        :param min_root: should be above the path or just the root directory,
        the user could not get deeper inside the directory tree than this
        :param only_dirs: if True it only shows directory entrys else you get files too
        :param hidden_path: means that hidden folders (and files too) will be shown
        :param include_all_sub: if set all subdirectorys are read and insert as objects
        - that causes a big carbage collection of tkinter objects when close the dialog
        :return: sg.TreeData() Object (But if you modify code, it is possible to get a dict or a list)
        """
        sg_treedata = sg.TreeData()
        pathstr = str(path)

        def has_sub_dir(some_path):
            """
            Check if a given path has subfolders
            :param some_path: could ne a string or a os.path
            :return: False if this has no subdirs otherwise True
            """
            subdir = False
            for file_or_folder in os.scandir(Path(some_path)):
                if file_or_folder.is_dir():
                    subdir = True
                    break
            return subdir

        def strip_path_of(cutoff, path_to_cut):
            """
            Just strip of the left substring of a string
            :param path_to_cut: A pathname to left cut
            :param cutoff: string to pop
            :return: the right tail of a string without the cutoff head
            """
            out = str(Path(path_to_cut).relative_to(Path(cutoff))).strip()
            if out == '.':
                out = ''
            return out

        def add_dirs_in_folder_fast(path_obj, dirs_only=False, pattern='', hidden=False, all_but_slow=False):
            """
            Recursive function that grab all files and folders of a given path
            :param pattern: filepattern like *.txt
            :param path_obj: An entry path object
            :param dirs_only: if set only folders will be given
            :param hidden: if set hidden files an folders are given too
            :param all_but_slow: if set subfolders of folder were scanned too
            :return: iterable data of all files and folders from a path
            """
            get_pattern = False
            patternlist = []
            try:
                if pattern:
                    get_pattern = True
                    patternlist = pattern.split()
                    for each in patternlist:
                        os.path.normcase(each)
            except TypeError:
                get_pattern = False
            try:
                for f in os.scandir(path_obj):
                    if f.is_dir():
                        if hidden or \
                                (not f.name.startswith('.') and not hidden):
                            if all_but_slow:
                                for eachdir in add_dirs_in_folder_fast(f.path, dirs_only, hidden, all_but_slow):
                                    yield eachdir
                                else:
                                    yield f.path
                            else:
                                yield f.path
                    if not only_dirs and f.is_file():
                        if hidden or \
                                (not f.name.startswith('.') and not hidden):
                            if not pattern or not get_pattern:
                                yield f.path
                            else:
                                for each in patternlist:
                                    if fnmatch.fnmatch(f.path, each):
                                        yield f.path

            except PermissionError:
                pass

        # create a string of the given path, so every entry in our list doesnt show that, and
        # every key in the list doesnt have that to have at all.
        head = pathstr
        # make a root entry in the list
        # pysimplegui seems to neet a empty string as root...
        # sg_treedata.Insert(strip_path_of(head, head), head, head, values=[], icon=folder_icon)
        if not (Path(head) == Path(min_root)):
            sg_treedata.Insert(strip_path_of(head, head), 'FOLDERUP', '.. Folder up', values=[], icon=folder_icon)

        # bad to say ,we need a list in memory to get all FileEntry
        # only if they are sorted, we could add the directorys in front of the files
        xlist = []
        # make a sorted list out of a dir.
        for eachpath in add_dirs_in_folder_fast(pathstr, pattern=findpattern, dirs_only=only_dirs,
                                                hidden=hidden_path, all_but_slow=include_all_sub):
            xlist.append(eachpath)
        # walk thru the list and insert the entry,s into the Treedata (for the treeview)
        for apath in sorted(xlist):
            parent = strip_path_of(head, str(Path(apath).parent))
            try:
                if Path(apath).is_dir():
                    sg_treedata.Insert(parent, strip_path_of(pathstr, str(apath)), str(Path(apath).name), values=[],
                                       icon=folder_icon)
                    # not visible placeholder just for shows that there are subfolders inside
                    if has_sub_dir(apath):  # insert a placeholder for subdirs
                        sg_treedata.Insert(strip_path_of(pathstr, str(apath)), '..', '..', values=[],
                                           icon=folder_icon)
                if Path(apath).is_file():
                    sg_treedata.Insert(parent, strip_path_of(pathstr, str(apath)), str(Path(apath).name), values=[],
                                       icon=file_icon)
            except KeyError:
                pass

        return sg_treedata

    actual_folder = initial_folder
    treedata = make_treedata_from_path(actual_folder, min_root=not_higher_than,
                                       findpattern=filepattern,
                                       only_dirs=directory_only, hidden_path=show_hidden,
                                       include_all_sub=with_all_subs)

    if theme not in sg.theme_list():
        theme = sg.theme_list()[0]
    new_theme = theme
    current_them = sg.theme()
    sg.ChangeLookAndFeel(new_theme)
    showmessage = message
    if showmessage == '':
        showmessage = actual_folder

    layout = [[sg.Text(showmessage)],
              [sg.Tree(data=treedata,
                       headings=['Size', ],
                       auto_size_columns=True,
                       num_rows=20,
                       col0_width=40,
                       key='-TREE-',
                       show_expanded=False,
                       enable_events=True),
               ],
              [sg.Button('Ok'), sg.Button('Cancel'), sg.Checkbox('Selection', change_submits=True, enable_events=True,
                                                                 key='selection_mode', visible=selection_mode)]]

    window = sg.Window(title, layout, icon=popup_icon)

    while True:  # Event Loop
        event, values = window.read()
        if event in ('-TREE-',):
            if not values['selection_mode']:
                if values['-TREE-'][0] == 'FOLDERUP':
                    actual_folder = Path(actual_folder).parent
                elif Path(os.path.join(actual_folder, treedata.tree_dict[values['-TREE-'][0]].text)).is_dir():
                    actual_folder = os.path.join(actual_folder, treedata.tree_dict[values['-TREE-'][0]].text)
                else:
                    continue
                showmessage = message
                if showmessage == '':
                    showmessage = actual_folder
                treedata = make_treedata_from_path(actual_folder, min_root=not_higher_than,
                                                   findpattern=filepattern,
                                                   only_dirs=directory_only, hidden_path=show_hidden,
                                                   include_all_sub=with_all_subs)

                layout1 = [[sg.Text(showmessage)],
                           [sg.Tree(data=treedata,
                                    headings=['Size', ],
                                    auto_size_columns=True,
                                    num_rows=20,
                                    col0_width=40,
                                    key='-TREE-',
                                    show_expanded=False,
                                    enable_events=True),
                            ],
                           [sg.Button('Ok'), sg.Button('Cancel'),
                            sg.Checkbox('Selection', change_submits=True, enable_events=True,
                                        key='selection_mode', visible=selection_mode)]]
                window.close()
                window = sg.Window(title, layout1)

        if event in (sg.WIN_CLOSED, 'Cancel'):
            break
        if event in ('Ok',):
            back = []
            for addpath in values['-TREE-']:
                back.append(os.path.join(actual_folder, addpath))
            break
    sg.ChangeLookAndFeel(current_them)
    window.close()
    return back


def do_args(arglist, __name__):
    """An argparser for commandline settings"""

    parser = argparse.ArgumentParser(description=__name__)
    parser.add_argument('-D', "--Directory_only", action="store_true",
                        help="Only able to select directorys", default=False)
    parser.add_argument('-m', '--message', action="store", dest="messagetext", type=str,
                        help="Message to display, none will display path.", required=False)
    parser.add_argument('-p', '--pattern', action="store", dest="patternlist", type=str,
                        help="Give some filepattern like *.txt, *.jpg.They could seperatet with comma or space.",
                        required=False)
    parser.add_argument('-t', '--title', action="store", dest="title", type=str,
                        help="Title of window", required=False)
    parser.add_argument('-l', '--look', action="store", dest="look", type=str,
                        help="String Name of a PySimpleGui Theme", required=False)
    parser.add_argument('-d', '--directory', action="store", dest="dir", type=str,
                        help="Initial directory to display.", required=False)
    parser.add_argument('-s', '--selection_mode', action="store_true",
                        help="Enable selection of directorys.", default=False)
    parser.add_argument('-r', '--rootdir', action="store", dest="rootdir", type=str,
                        help="Up to which folder the user could browse", required=False)
    return parser.parse_args(arglist)


def main():
    """The Main Routine, if this is not uses as a Modul
    Make it executable and run it like:
    python ./DirBrowserPopup.py -h
    Sample:
    python ./DirBrowserPopup.py -D -m "My Message" -t "My Dirselector" -l "DarkAmber" -d ~/
    which gives you a nice folder browser of your home folder
    """
    args = do_args(sys.argv[1:], __name__)
    message = args.messagetext
    if not message:
        message = ''
    title = args.title
    look = args.look
    if not look:
        look = 'LightGreen5'
    dirs = args.dir
    if not dirs:
        dirs = str(Path.home())
    rootdir = args.rootdir
    if not rootdir:
        rootdir = str(Path('/'))
    dir_only = args.Directory_only
    fileendings = args.patternlist
    selection = args.selection_mode = True
    demo = PopupGetFileOrFolder(message=message, title=title,
                                initial_folder=dirs,
                                not_higher_than=Path.home(),  # or rootdir or whatever
                                filepattern=fileendings,
                                theme=look,
                                directory_only=dir_only,
                                selection_mode=selection,
                                show_hidden=False,
                                with_all_subs=False)  # with all subs !! SLOWS SPEED when close the dialog!!!
    return demo


if __name__ == '__main__':
    listoffiles = main()
    if listoffiles:
        try:
            for each in listoffiles:
                print(each)
        except (BrokenPipeError, IOError):
            pass
        sys.stderr.close()
        # to print out the list do print(main())
