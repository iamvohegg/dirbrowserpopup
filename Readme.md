# DirBrowser

PopupGetFileOrFolder() for PySimpeGUI

## Getting Started
Use this like other Popups
```    
    demo = PopupGetFileOrFolder(message=message, title=title,
                                initial_folder=dirs,
                                not_higher_than=Path.home(), # or rootdir or whatever
                                theme=look,
                                directory_only=dir_only,
                                show_hidden=False,
                                with_all_subs=False)  # with all subs !! SLOWS SPEED when close the dialog!!!
```
### Look like

![Popup Dialog](Picture.png)

### Prerequisites

requires: PySimpleGUI 

```
pip install pysimplegui
or
pip3 install pysimplegui
```


## Running the tests

no tests inside yet.
Could use it from the commandline but first make it executable `chmod +x DirBrowser.py`
Then:

- `./DirBrowser.py -h`  shows you help.
```
usage: DirBrowser.py [-h] [-D] [-m MESSAGETEXT] [-t TITLE] [-l LOOK] [-d DIR] [-s] [-r ROOTDIR]

__main__

optional arguments:
  -h, --help            show this help message and exit
  -D, --Directory_only  Only able to select directorys
  -m MESSAGETEXT, --message MESSAGETEXT
                        Message to display, none will display path.
  -t TITLE, --title TITLE
                        Title of window
  -l LOOK, --look LOOK  String Name of a PySimpleGui Theme
  -d DIR, --directory DIR
                        Initial directory to display.
  -s, --selection_mode  Enable selection of directorys.
  -r ROOTDIR, --rootdir ROOTDIR
                        Up to which folder the user could browse
```
Call it like:
`./DirBrowser.py -D -m 'Please select a folder' -t 'MyFolderSelector' -l 'LightGreen2' -d ~/`
gives the home folder to you with the option to select files AND folders by simple click or many of them by `shift`-click.

## Deployment

So just put:
```
    demo = PopupGetFileOrFolder(message='',
                                title='Folder selection',
                                initial_folder='/home/user',
                                not_higher_than='/home',
                                theme='Default',
                                directory_only=True,
                                show_hidden=False,
                                with_all_subs=False)
    print('Here what user seleced : {}.'.format(demo))

```                                
## Built With

* [Python](https://www.python.org/) - what else?
* [PySimpleGUI](https://pysimplegui.readthedocs.io/en/latest/) - Read the Docs

## Authors

* **Volker Heggemann** - *work on this* - [vohe](https://gitlab.com/)
* **PySimpleGUI** - *mostly done by* - [The PySimpleGUI Organization](https://github.com/PySimpleGUI/PySimpleGUI)
* **there are many others** *See also* [contributors](https://github.com/PySimpleGUI/PySimpleGUI/graphs/contributors) who participated in PySimpeGUI project.

## License

My project is licensed under the CC4.0-BY License - see the [creative commons](https://creativecommons.org/licenses/by/4.0/) file for details
PySimpleGUI is licensed under another License - see [License.txt](https://github.com/PySimpleGUI/PySimpleGUI/blob/master/license.txt) file for details 

## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration
* etc
