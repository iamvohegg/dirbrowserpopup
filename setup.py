from setuptools import setup

setup(
    name='DirBrowserPopup',
    version='1.06',
    packages=['PySimpleGUI'],
    url='',
    license='CC4.0  BY',
    author='Volker Heggemann',
    author_email='v.hegg @ gmail.com',
    description='Directory and File Browser for PySimpeGUI'
)
